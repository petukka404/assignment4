# CT70A3000 Software Maintenance
# Author: Petri Rämö 0438578
# Date: 19.01.2019

# this is the file where the test will fail, where i set the counter on how may numbers there is starting from 1

def fibo(a, b):
    
    if isinstance(a, int) != True or isinstance(b, int) != True or a < b:
        return "wrong"
    else:    
        f = open("output.txt", "w")
        c, d = 0, 1
        e = 1
        z = 1
        while z == 1:

            if a == 0:
                z = 0

            if b <= c and a != 0:
                e = e + 1
            a = a - 1
            c, d = d, c+d
        f.write(str(e))
        f.close()

        return e
