# CT70A3000 Software Maintenance
# Author: Petri Rämö 0438578
# Date: 19.01.2019

import unittest
from fibofail import fibo

#References: http://pythontesting.net/framework/unittest/unittest-introduction/#more-334
#this is the test file that will give failures

class Test(unittest.TestCase):

    def test_5_2(self):
        self.assertEqual(fibo(5, 2), 2)

    def test_2_5(self):
        self.assertEqual(fibo(2, 5), "wrong")

    def test_integer(self):
        self.assertEqual(fibo("lul", 6), "wrong")
        self.assertEqual(fibo(6, "lul"), "wrong")
    
        

if __name__ == "__main__":
    unittest.main()