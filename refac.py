# CT70A3000 Software Maintenance
# Author: Petri Rämö 0438578
# Date: 08.02.2019


import rope.base.project
from rope.base import libutils
from rope.refactor.extract import ExtractVariable
from rope.refactor.rename import Rename

project = rope.base.project.Project('fibo')
resource = libutils.path_to_resource(project, 'fibo/fibo.py')
# extractor = ExtractVariable(project, resource, start, end)

# this checks if the file is python file
if True == libutils.is_python_file(project, resource):
    print("ok")

# changes the variable f to be named file
changes = Rename(project, resource, offset=396).get_changes('file')
project.do(changes)

